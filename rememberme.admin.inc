<?php

/**
 * @file
 * Menu callback include file for admin/settings pages.
 */

/**
 * Form generator callback for our settings.
 */
function rememberme_settings_form($form, &$form_state) {
  $default_alter_forms = array('user_login', 'user_login_block');
  $alter_forms = variable_get('rememberme_alter_forms', $default_alter_forms);
  $form['rememberme_alter_forms'] = array(
    '#type' => 'texteara',
    '#default_value' => explode("\n", $alter_forms),
    '#title' => t('Forms to attach "Remember me" to'),
    '#description' => t('A list of form identifiers to attach to, one per line'),
  );
  $form['rememberme_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default state for the "Remember me" checkbox'),
    '#default_value' => variable_get('rememberme_default', 0),
  );
  $form['rememberme_cookie_lifetime'] = array(
    '#type' => 'textfield',
    '#title' => t('Session duration'),
    '#description' => t('Duration of the cookie lifetime when the "Remember me" checkbox is checked, in seconds'),
    '#default_value' => variable_get('rememberme_cookie_lifetime', 3600 * 24 * 30),
  );
  $default_title = t('Remember this computer for 30 days');
  $title = variable_get('rememberme_title', $default_title);
  $form['rememberme_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Checkbox title'),
    '#description' => t('The text that is displayed to users on login form.'),
    '#default_value' => $title,
  );
  return system_settings_form($form);
}
